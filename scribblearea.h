/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef SCRIBBLEAREA_H
#define SCRIBBLEAREA_H

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <QString>
#include <QObject>
#include <QUndoCommand>


class abstractTool;
class UndoCommand;

//! [0]
class ScribbleArea : public QWidget
{
    Q_OBJECT


public:
    ScribbleArea(QWidget *parent = 0);

    bool openImage(const QString &fileName);
    bool saveImage(const QString &fileName, const char *fileFormat);
    void setPenColor(const QColor &newColor);
    void setPenWidth(int newWidth);
    void setEraserWidth(int newWidth);

    bool isModified() const { return modified; }
    QColor penColor() const { return myPenColor; }
    int penWidth() const { return myPenWidth; }
    int eraserWidth() const {return myEraserWidth;}


public slots:
    void clearImage();
    void print();

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);

public:
    void drawLineTo(const QPoint &endPoint);
    void drawLine(const QPoint &endPoint);
    void drawRectTo(const QPoint &endPoint);
    void drawEllipseTo(const QPoint &endPoint);
    void drawCircleTo(const QPoint &endPoint);
    void eraseTo(const QPoint &endPoint);
    void drawText(const QPoint &endPoint, QString &text);

    void resizeImage(QImage *image, const QSize &newSize);

    bool modified;
    bool scribbling;
    int myPenWidth;
    int myEraserWidth;
    QColor myPenColor;
    QImage image;
    QPoint lastPoint;
    // QPoint endPoint;
    QUndoStack *mUndoStack;

public:
    void pushUndoCommand(UndoCommand *command);

public:
    abstractTool *currentTool;

    void setCurrentTool(abstractTool*);

};
//! [0]

class abstractTool: public QObject{
    Q_OBJECT

public:
    QImage mImageCopy;

    explicit abstractTool(QObject *parent = 0);

    virtual void mousePressEvent(QMouseEvent *event, ScribbleArea &area) {};
    virtual void mouseMoveEvent(QMouseEvent *event, ScribbleArea &area) {};
    virtual void mouseReleaseEvent(QMouseEvent *event, ScribbleArea &area) {};
    // virtual void paintEvent(QPaintEvent *event, ScribbleArea &area) {};
    virtual void makeUndoCommand(ScribbleArea &area);
};

class PenTool: public abstractTool{
    Q_OBJECT
public:
    explicit PenTool(QObject *parent = 0);
    void mousePressEvent(QMouseEvent *event, ScribbleArea &area);
    void mouseMoveEvent(QMouseEvent *event, ScribbleArea &area);
    void mouseReleaseEvent(QMouseEvent *event, ScribbleArea &area);
};

class RectTool: public abstractTool{
    Q_OBJECT

public:
    explicit RectTool(QObject *parent = 0);
    void mousePressEvent(QMouseEvent *event, ScribbleArea &area);
    void mouseMoveEvent(QMouseEvent *event, ScribbleArea &area);
    void mouseReleaseEvent(QMouseEvent *event, ScribbleArea &area);
};

class EllipseTool: public abstractTool{
    Q_OBJECT

public:
    explicit EllipseTool(QObject *parent = 0);
    void mousePressEvent(QMouseEvent *event, ScribbleArea &area);
    void mouseMoveEvent(QMouseEvent *event, ScribbleArea &area);
    void mouseReleaseEvent(QMouseEvent *event, ScribbleArea &area);
};

class CircleTool: public abstractTool{
    Q_OBJECT

public:
    explicit CircleTool(QObject *parent = 0);
    void mousePressEvent(QMouseEvent *event, ScribbleArea &area);
    void mouseMoveEvent(QMouseEvent *event, ScribbleArea &area);
    void mouseReleaseEvent(QMouseEvent *event, ScribbleArea &area);
};

class FillTool: public abstractTool{
    Q_OBJECT

public:
    explicit FillTool(QObject *parent = 0);
    void mousePressEvent(QMouseEvent *event, ScribbleArea &area);
    void mouseMoveEvent(QMouseEvent *event, ScribbleArea &area);
    void mouseReleaseEvent(QMouseEvent *event, ScribbleArea &area);
    void fill(ScribbleArea &area);
    void fillRecur(int x, int y, QRgb switchColor, QRgb originalColor, QImage &tmpImage);
};

class LineTool: public abstractTool{
    Q_OBJECT
public:
    QLine mline;
    QImage tmpImage;
    bool signal;

    explicit LineTool(QObject *parent = 0);
    void mousePressEvent(QMouseEvent *event, ScribbleArea &area);
    void mouseMoveEvent(QMouseEvent *event, ScribbleArea &area);
    void mouseReleaseEvent(QMouseEvent *event, ScribbleArea &area);
    // void paintEvent(QPaintEvent *event, ScribbleArea &area);
};

class EraserTool: public abstractTool{
    Q_OBJECT
public:
    explicit EraserTool(QObject *parent = 0);
    void mousePressEvent(QMouseEvent *event, ScribbleArea &area);
    void mouseMoveEvent(QMouseEvent *event, ScribbleArea &area);
    void mouseReleaseEvent(QMouseEvent *event, ScribbleArea &area);
};

class TextTool: public abstractTool{
    Q_OBJECT
public:
    explicit TextTool(QObject *parent = 0);
    void mousePressEvent(QMouseEvent *event, ScribbleArea &area);
    void mouseMoveEvent(QMouseEvent *event, ScribbleArea &area);
    void mouseReleaseEvent(QMouseEvent *event, ScribbleArea &area);
};

class UndoCommand: public QUndoCommand{
public:
    UndoCommand(const QImage &img, ScribbleArea &area, QUndoCommand *parent = 0);
    virtual void undo();
    virtual void redo();
private:
    QImage mPrev;
    QImage mCurr;
    ScribbleArea &area;
};

#endif

/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtWidgets>
#include <QDebug>
#ifndef QT_NO_PRINTER
#include <QPrinter>
#include <QPrintDialog>
#endif

#include "scribblearea.h"

//! [0]
ScribbleArea::ScribbleArea(QWidget *parent)
    : QWidget(parent)
{
    setAttribute(Qt::WA_StaticContents);
    modified = false;
    scribbling = false;
    myPenWidth = 1;
    myPenColor = Qt::blue;
    mUndoStack = new QUndoStack;
    currentTool = new LineTool;
}
//! [0]

//! [1]
bool ScribbleArea::openImage(const QString &fileName)
//! [1] //! [2]
{
    QImage loadedImage;
    if (!loadedImage.load(fileName))
        return false;

    QSize newSize = loadedImage.size().expandedTo(size());
    resizeImage(&loadedImage, newSize);
    image = loadedImage;
    modified = false;
    update();
    return true;
}
//! [2]

//! [3]
bool ScribbleArea::saveImage(const QString &fileName, const char *fileFormat)
//! [3] //! [4]
{
    QImage visibleImage = image;
    resizeImage(&visibleImage, size());

    if (visibleImage.save(fileName, fileFormat)) {
        modified = false;
        return true;
    } else {
        return false;
    }
}
//! [4]

//! [5]
void ScribbleArea::setPenColor(const QColor &newColor)
//! [5] //! [6]
{
    myPenColor = newColor;
}
//! [6]

//! [7]
void ScribbleArea::setPenWidth(int newWidth)
//! [7] //! [8]
{
    myPenWidth = newWidth;
}
//! [8]
//!
void ScribbleArea::setEraserWidth(int newWidth){
    myEraserWidth = newWidth;
}

//! [9]
void ScribbleArea::clearImage()
//! [9] //! [10]
{
    image.fill(qRgb(255, 255, 255));
    modified = true;
    update();
}
//! [10]

//! [11]
void ScribbleArea::mousePressEvent(QMouseEvent *event)
//! [11] //! [12]
{
    /*
    if (event->button() == Qt::LeftButton) {
        lastPoint = event->pos();
        scribbling = true;
    }
    */

    currentTool->mousePressEvent(event, *this);
}

abstractTool::abstractTool(QObject *parent): QObject(parent) {}

PenTool::PenTool(QObject *parent): abstractTool(parent) {}

void PenTool::mousePressEvent(QMouseEvent *event, ScribbleArea &area){
    if (event->button() == Qt::LeftButton) {
        area.lastPoint = event->pos();
        area.scribbling = true;
    }
}

void ScribbleArea::mouseMoveEvent(QMouseEvent *event)
{
    /*
    if ((event->buttons() & Qt::LeftButton) && scribbling)
        drawLineTo(event->pos());
        */
    currentTool->mouseMoveEvent(event, *this);
}

void PenTool::mouseMoveEvent(QMouseEvent *event, ScribbleArea &area){
    if ((event->buttons() & Qt::LeftButton) && area.scribbling)
        area.drawLineTo(event->pos());
}

void ScribbleArea::mouseReleaseEvent(QMouseEvent *event)
{
    /*
    if (event->button() == Qt::LeftButton && scribbling) {
        drawLineTo(event->pos());
        scribbling = false;
    }
    */
    currentTool->mouseReleaseEvent(event, *this);
}

void PenTool:: mouseReleaseEvent(QMouseEvent *event, ScribbleArea &area){
    if (event->button() == Qt::LeftButton && area.scribbling) {
        area.drawLineTo(event->pos());
        area.scribbling = false;
    }
}

//! [12] //! [13]
void ScribbleArea::paintEvent(QPaintEvent *event)
//! [13] //! [14]
{

    QPainter painter(this);
    QRect dirtyRect = event->rect();
    painter.drawImage(dirtyRect, image, dirtyRect);

    // currentTool->paintEvent(event, *this);
}
//! [14]

//! [15]
void ScribbleArea::resizeEvent(QResizeEvent *event)
//! [15] //! [16]
{
    if (width() > image.width() || height() > image.height()) {
        int newWidth = qMax(width() + 128, image.width());
        int newHeight = qMax(height() + 128, image.height());
        resizeImage(&image, QSize(newWidth, newHeight));
        update();
    }
    QWidget::resizeEvent(event);
}
//! [16]

//! [17]
void ScribbleArea::drawLineTo(const QPoint &endPoint)
//! [17] //! [18]
{
    qDebug()<<"Drawing line";
    QPainter painter(&image);
    painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap,
                        Qt::RoundJoin));
    painter.drawLine(lastPoint, endPoint);
    modified = true;

    int rad = (myPenWidth / 2) + 2;
    update(QRect(lastPoint, endPoint).normalized()
                                     .adjusted(-rad, -rad, +rad, +rad));
    lastPoint = endPoint;
}
//! [18]
//!
void ScribbleArea::eraseTo(const QPoint &endPoint){
    QPainter painter(&image);
    qDebug()<<"erasing";
    painter.setPen(QPen(Qt::white,
                        myEraserWidth,
                        Qt::SolidLine,
                        Qt::RoundCap,
                        Qt::RoundJoin));
    painter.drawLine(lastPoint, endPoint);
    modified = true;
    int rad = (myPenWidth / 2) + 2;
    update(QRect(lastPoint, endPoint).normalized()
           .adjusted(-rad, -rad, +rad, +rad));
    lastPoint = endPoint;
}

//!
void ScribbleArea::drawRectTo(const QPoint &endPoint){
    qDebug()<<"Drawing Rect";
    QPainter painter(&image);
    painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap,
                        Qt::RoundJoin));
    painter.drawRect(QRect(lastPoint, endPoint));
    modified = true;

    int rad = (myPenWidth / 2) + 2;
    // update(QRect(lastPoint, endPoint).normalized()
    //                                 .adjusted(-rad, -rad, +rad, +rad));
    update();
    // lastPoint = endPoint;
}

void ScribbleArea::drawEllipseTo(const QPoint &endPoint){
    qDebug()<<"Drawing Ellipse";
    QPainter painter(&image);
    painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap,
                        Qt::RoundJoin));

    painter.drawEllipse(QRect(lastPoint, endPoint));
    modified = true;

    // int rad = (myPenWidth / 2) + 2;
    update();
    // lastPoint = endPoint;
}

void ScribbleArea::drawCircleTo(const QPoint &endPoint){
    qDebug()<<"Drawing circle";
    QPainter painter(&image);
    painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap,
                        Qt::RoundJoin));
    int radius = QLineF(lastPoint, endPoint).length();
    painter.drawEllipse(lastPoint, radius, radius);
    modified = true;

    update();

    // lastPoint = endPoint;
}

void ScribbleArea::drawLine(const QPoint &endPoint){
    QPainter painter(&image);
    painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap,
                        Qt::RoundJoin));
    painter.drawLine(lastPoint, endPoint);
    modified = true;

    int rad = (myPenWidth / 2) + 2;

    // update(QRect(lastPoint, endPoint).normalized()
    //                                 .adjusted(-rad, -rad, +rad, +rad));
    update();
}

void ScribbleArea::drawText(const QPoint &endPoint, QString &text){
    QPainter painter(&image);
    painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap,
                        Qt::RoundJoin));
    painter.drawText(endPoint, text);
    int rad = (myPenWidth / 2) + 2;
    update(QRect(lastPoint, endPoint).normalized()
                                     .adjusted(-rad, -rad, +rad, +rad));
    modified = true;

}

//! [19]
void ScribbleArea::resizeImage(QImage *image, const QSize &newSize)
//! [19] //! [20]
{
    if (image->size() == newSize)
        return;

    QImage newImage(newSize, QImage::Format_RGB32);
    newImage.fill(qRgb(255, 255, 255));
    QPainter painter(&newImage);
    painter.drawImage(QPoint(0, 0), *image);
    *image = newImage;
}
//! [20]

//! [21]
void ScribbleArea::print()
{
#if !defined(QT_NO_PRINTER) && !defined(QT_NO_PRINTDIALOG)
    QPrinter printer(QPrinter::HighResolution);

    QPrintDialog printDialog(&printer, this);
//! [21] //! [22]
    if (printDialog.exec() == QDialog::Accepted) {
        QPainter painter(&printer);
        QRect rect = painter.viewport();
        QSize size = image.size();
        size.scale(rect.size(), Qt::KeepAspectRatio);
        painter.setViewport(rect.x(), rect.y(), size.width(), size.height());
        painter.setWindow(image.rect());
        painter.drawImage(0, 0, image);
    }


#endif // QT_NO_PRINTER
}
//! [22]


RectTool::RectTool(QObject *parent): abstractTool(parent){}

void RectTool:: mousePressEvent(QMouseEvent *event, ScribbleArea &area){
    if(event->button() == Qt::LeftButton){
        area.lastPoint = event->pos();
        area.scribbling = true;

        mImageCopy = area.image;

        makeUndoCommand(area);
    }
}

void RectTool::mouseMoveEvent(QMouseEvent *event, ScribbleArea &area){
    // qDebug() << "mouse moving during rect drawing";
    if( event->type() == QEvent::MouseMove && area.scribbling){
        //area.drawRectTo(event->pos());
        area.image = mImageCopy;
        area.drawRectTo(event->pos());
        qDebug() << "mouse moving ";
    }
}

void RectTool::mouseReleaseEvent(QMouseEvent *event, ScribbleArea &area){
    if(event->button() == Qt::LeftButton && area.scribbling){
        area.drawRectTo(event->pos());
        qDebug()<<event->pos();
        area.scribbling = false;
    }
}

EllipseTool::EllipseTool(QObject *parent): abstractTool(parent){}

void EllipseTool:: mousePressEvent(QMouseEvent *event, ScribbleArea &area){
    if(event->button() == Qt::LeftButton){
        area.lastPoint = event->pos();
        mImageCopy = area.image;
        area.scribbling = true;
        makeUndoCommand(area);
    }
}

void EllipseTool::mouseMoveEvent(QMouseEvent *event, ScribbleArea &area){
    qDebug() << "mouse moving during ellipse drawing";
    if(event->type() == QEvent::MouseMove && area.scribbling){
        area.image = mImageCopy;
        area.drawEllipseTo(event->pos());
        qDebug() << "left Button of ellipse drawing";
    }
}

void EllipseTool::mouseReleaseEvent(QMouseEvent *event, ScribbleArea &area){
    if(event->button() == Qt::LeftButton && area.scribbling){
        area.drawEllipseTo(event->pos());
        qDebug()<<event->pos();
        area.scribbling = false;
    }
}

CircleTool::CircleTool(QObject *parent): abstractTool(parent){}

void CircleTool:: mousePressEvent(QMouseEvent *event, ScribbleArea &area){
    if(event->button() == Qt::LeftButton){
        area.lastPoint = event->pos();
        mImageCopy = area.image;
        area.scribbling = true;
        makeUndoCommand(area);
    }
}

void CircleTool::mouseMoveEvent(QMouseEvent *event, ScribbleArea &area){
    qDebug() << "mouse moving during circle drawing";
    if(event->type() == QEvent::MouseMove && area.scribbling){
        area.image = mImageCopy;
        area.drawCircleTo(event->pos());
        qDebug() << "left Button of circle drawing";
    }
}

void CircleTool::mouseReleaseEvent(QMouseEvent *event, ScribbleArea &area){
    if(event->button() == Qt::LeftButton && area.scribbling){
        area.drawCircleTo(event->pos());
        qDebug()<<event->pos();
        area.scribbling = false;
    }
}

FillTool::FillTool(QObject *parent):abstractTool(parent) {}

void FillTool::mousePressEvent(QMouseEvent *event, ScribbleArea &area){
    if(event->button() & Qt::LeftButton){
        area.lastPoint = event->pos();
        makeUndoCommand(area);
    }
}

void FillTool::mouseMoveEvent(QMouseEvent *event, ScribbleArea &area){

}

void FillTool::mouseReleaseEvent(QMouseEvent *event, ScribbleArea &area){
    if(event->button() == Qt::LeftButton){
        fill(area);
    }
}

void FillTool::fill(ScribbleArea &area){
    QColor switchColor;
    switchColor = area.myPenColor;
    QRgb pixel(area.image.pixel(area.lastPoint));
    QColor originalColor(pixel);
    if(switchColor != originalColor){
        fillRecur(area.lastPoint.x(), area.lastPoint.y(),
                  switchColor.rgb(), originalColor.rgb(),
                  area.image);
    }
    area.modified = true;
    area.update();
}

void FillTool::fillRecur(int x, int y, QRgb switchColor, QRgb originalColor, QImage &tmpImage){
    int tmpX(x), leftX(0);
    while(true){
        if(tmpImage.pixel(tmpX, y) != originalColor)
            break;
        tmpImage.setPixel(tmpX, y, switchColor);
        if(tmpX > 0){
            --tmpX;
            leftX = tmpX;
        }
        else{
            break;
        }
    }

    int rightX(0);
    tmpX = x + 1;
    while(true){
        if(tmpImage.pixel(tmpX, y) != originalColor){
            break;
        }
        tmpImage.setPixel(tmpX, y, switchColor);
        if(tmpX < tmpImage.width() - 1){
            tmpX++;
            rightX = tmpX;
        }
        else{
            break;
        }
    }

    for(int x_(leftX + 1); x_ < rightX; ++x_){
        if(y < 1 || y >= tmpImage.height() - 1){
            break;
        }
        if(rightX > tmpImage.width()){
            break;
        }
        QRgb currentColor = tmpImage.pixel(x_, y-1);
        if(currentColor == originalColor && currentColor != switchColor){
            fillRecur(x_, y-1, switchColor, originalColor, tmpImage);
        }
        currentColor = tmpImage.pixel(x_, y+1);
        if(currentColor == originalColor && currentColor != switchColor){
            fillRecur(x_, y+1, switchColor, originalColor, tmpImage);
        }

    }
}

LineTool::LineTool(QObject *parent):abstractTool(parent) {
    // tmpImage= QImage(400, 400);
    // signal = false;
}

void LineTool::mousePressEvent(QMouseEvent *event, ScribbleArea &area){
    if(event->button() == Qt::LeftButton){
        area.lastPoint = event->pos();
        area.scribbling = true;

        mImageCopy = area.image;
        // area.endPoint = event->pos();
        qDebug()<<"Line tool mouse pressed";
        // mline.setP1(event->pos());
        // mline.setP2(event->pos());
        makeUndoCommand(area);
    }
}

void LineTool::mouseMoveEvent(QMouseEvent *event, ScribbleArea &area){
    if(event->type() == QEvent::MouseMove && area.scribbling){
        qDebug()<<"line tool mouse moving";
        area.image = mImageCopy;
        area.drawLine(event->pos());

    }
    // area.update();
}

void LineTool::mouseReleaseEvent(QMouseEvent *event, ScribbleArea &area){
    if(event->button() == Qt::LeftButton && area.scribbling){
        area.drawLine(event->pos());
        // area.image = mImageCopy;
        area.scribbling = false;
        qDebug()<<"Line tool  mouse released";
        // area.scribbling = false;
        // area.update();
    }
}

/*
void LineTool::paintEvent(QPaintEvent *event, ScribbleArea &area){
    static bool wasPressed = false;
    QPainter painter(&area.image);
    painter.setPen(QPen(area.myPenColor, area.myPenWidth,
                        Qt::SolidLine, Qt::RoundCap,
                        Qt::RoundJoin ));
    qDebug()<<"Drawing Line";
    if(area.scribbling){
        qDebug()<<"reDrawing";
        painter.drawImage(0, 0, area.image);
        painter.drawLine(mline);
        qDebug()<<mline.p1();
        qDebug()<<mline.p2();
        wasPressed = true;
    }
    else if(wasPressed){
        qDebug()<<"wasPressed";
        painter.drawLine(mline);
        qDebug()<<mline.p1()<<mline.p2();

        painter.drawImage(0, 0, area.image);
        wasPressed = false;
    }
    // painter.drawLine();
}
*/

EraserTool::EraserTool(QObject *parent):abstractTool(parent) {}

void EraserTool::mousePressEvent(QMouseEvent *event, ScribbleArea &area){
    if(event->button() == Qt::LeftButton){
        area.lastPoint = event->pos();
        area.scribbling = true;
    }
}

void EraserTool::mouseMoveEvent(QMouseEvent *event, ScribbleArea &area){
    if((event->buttons() & Qt::LeftButton)  && area.scribbling){
        qDebug()<<"mouse moving during erasing";
        area.eraseTo(event->pos());
    }
}

void EraserTool::mouseReleaseEvent(QMouseEvent *event, ScribbleArea &area){
    if(event->button() == Qt::LeftButton && area.scribbling){
        area.eraseTo(event->pos());
        area.scribbling = false;
    }
}

TextTool::TextTool(QObject *parent):abstractTool(parent) {
    // mText = QString();
}

void TextTool::mousePressEvent(QMouseEvent *event, ScribbleArea &area) {
    makeUndoCommand(area);
}

void TextTool::mouseMoveEvent(QMouseEvent *event, ScribbleArea &area) {}

void TextTool::mouseReleaseEvent(QMouseEvent *event, ScribbleArea &area){
    bool ok;
    QString text = QInputDialog::getText(0, tr("Text Tool"),
                                         tr("insert text: "),
                                         QLineEdit::Normal,
                                         QDir::home().dirName(), &ok);
    if(ok && !text.isEmpty()){
        area.drawText(event->pos(), text);

    }
}

UndoCommand::UndoCommand(const QImage &img, ScribbleArea &Area, QUndoCommand *parent):
    QUndoCommand(parent), mPrev(img), area(Area){
    mCurr= mPrev;
}

void UndoCommand::undo(){
    mCurr = area.image;
    area.image = mPrev;
    area.update();
}

void UndoCommand::redo(){
    area.image = mCurr;
    area.update();
}

void abstractTool::makeUndoCommand(ScribbleArea &area){
    area.pushUndoCommand(new UndoCommand(area.image,  area));
}

void ScribbleArea::pushUndoCommand(UndoCommand *command){
    if(command != 0){
        mUndoStack->push(command);
    }
}

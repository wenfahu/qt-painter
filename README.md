# README #

Qt Application which supports basic painting functionalities(similar to MS painter) based on Qt's [scribble example](http://doc.qt.io/qt-5/qtwidgets-widgets-scribble-example.html) and [EasyPaint](http://qt-apps.org/content/show.php/EasyPaint?content=140877) 

### Dependencies ###

* QMake version 3.0
* Qt version 5.2.0

### How do I get set up? ###

* using Qt creator
    * open the `.pro` file in your Qt creator and run the project

* using QMake
    * `cd` to the path of the project 
    * `mkdir ./build`
    * `cd build`
    * `qmake ../scribble.pro`
    * `make` 